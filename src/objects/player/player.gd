extends KinematicBody2D

export (int) var speed = 12000
export (int) var jump_speed = -1800
export (int) var gravity = 4000
var walk_speed = speed / 5
var velocity = Vector2.ZERO

onready var animation = $AnimationPlayer
onready var sprite = $Sprite
func _ready():
	pass


func _physics_process(delta):
	velocity.x = 0
	playerAction(delta)
	velocity = move_and_slide(velocity, Vector2.UP)
		
func playerAction(delta):
	if Input.is_action_pressed("player_run_right"):
		sprite.flip_h = false
		animation.play("run")
		velocity.x += speed * delta
	elif Input.is_action_pressed("player_run_left"):
		sprite.flip_h = true
		animation.play("run")
		velocity.x -= speed * delta
	elif Input.is_action_pressed("player_right"):
		sprite.flip_h = false
		animation.play("walk")
		velocity.x += walk_speed * delta
	elif Input.is_action_pressed("player_left"):
		sprite.flip_h = true
		animation.play("walk")
		velocity.x -= walk_speed * delta
	else:
		animation.play("idle")
